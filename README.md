myfind is a Perl program used to search through directories using the command line. 
Perl is required. To use the program commands with a mix of the following arguments can be used:

perl /path/to/crouse.pl ~/Documents -name "myFile" (Search for a specific file name)
perl /path/to/crouse.pl ~/Documents -name "*.jpg" (Returns all .jpg files)
perl /path/to/crouse.pl ~/Documents -grep "pizza" (this will return all files where the word "pizza" is found in the text of the documents)

perl /path/to/crouse.pl ~/Documents -ls (Gives permissions and information of all files in ~/Documents) 
