#! /usr/local/bin/perl

#Charles Rouse
#CS 312
#myfind.pl

use strict;
use warnings;
use Cwd;

#Declare variables that will be set to True or False based on command line arguments
my $arg_grep = 0;     
my $arg_ls = 0;       
my $arg_name = 0;     
my $arg_pwd = 0;      
my $grep_search_string = 0;  #Search string used after -grep (i.e. print)
my $wildcard = 0;     #Argument that follows -name (i.e. \".pl\")

#ARGV arguments from command line. If found, argument set to 1.
foreach my $args(@ARGV) {

    if($args eq "-grep") {
        #$arg_grep = 1;  #Fan
        $arg_grep = 2;  
    }

    elsif($args eq "-ls") {
        $arg_ls = 1;
    }

    elsif($args eq "-name") {
       # $arg_name = 1;  #Fan
       $arg_name = 2;

    }


    elsif($args eq "-pwd") {
        $arg_pwd = 1;
    }
    
    # elsif($args =~ s/"//g) {   #Fan
    #     $wildcard = $args;
    # }

    #else {
    elsif($arg_grep == 2){   #Fan
        $grep_search_string = $args;
	    $grep_search_string =~ s/"//g;
        $arg_grep = 1;
    }

    #Added by Fan
    elsif($arg_name == 2){
         $wildcard = $args;
         $arg_name = 1;
    }

    else{}
}

#Glob function provided in link
sub glob2pat {
    my $globstr = shift;
    my %patmap = (
    '*' => '.*',
    '?' => '.',
    '[' => '[',
    ']' => ']',
    );
    $globstr =~ s{(.)} { $patmap{$1} || "\Q$1" }ge;
    return '^' . $globstr . '$';
}

=pod
Recursively scans directories and prints folders. Output is determined through a series of if statements that check which arguments have been provided through the command line.
=cut
sub scan_directory {
    my ($workdir) = shift;           
    my ($startdir) = &cwd;           
    
    chdir($workdir) or die "Unable to enter dir $workdir:$!\n";
    
    opendir(DIR, ".") or die "Unable to open $workdir:$!\n";

    my @names = readdir(DIR) or die "Unable to read $workdir:$!\n";

    closedir(DIR);

    
    foreach my $name (@names) {
        next if ($name eq ".");
        next if ($name eq "..");
        
	#Recursive call to scan subdirectory if $name is subdirectory
        if (-d $name) {     
            &scan_directory($name);
            next;
        }
        
        if (@ARGV == 1) {
            print "$name\n";
        }
        
	#Else $name is not file
        else {
  #Fan:  too many if conditions to isolate argument combinations
#Begin if statements to check which arguments were used
            if(-f $name) {

                if($arg_ls == 1) {

                    if($wildcard) {

                        if($name =~ &glob2pat($wildcard)) {
                            system("ls -l $name ");
                        }
                    }

                    else {
                        system("ls -l $name ");
                    }
                }
     
            }

            if($arg_pwd == 1) {

                if($wildcard && $arg_grep == 0) {

                    if($name =~ &glob2pat($wildcard)) {

                        my $filepath = cwd . "/" . $name;
                        print "$filepath\n";
                    }
                }

                if(!$wildcard && $arg_grep == 0) {
                    my $filepath = cwd . "/" . $name;
                    print "$filepath\n";
                    }
            }

            if($arg_name == 1) {

                if($arg_ls == 0 && $arg_pwd == 0) {

                    if($wildcard && $arg_grep == 0) {

                        if($name =~ &glob2pat($wildcard)) {
                            print " $name\n";
                        }
                    }

                    elsif($wildcard && $arg_grep == 0) {
                        print " $name\n";
                    }
                }
            }
            
#-grep command variations	    
            if ($arg_grep == 1 && !$wildcard && $arg_pwd == 0) {
                    open (FILE, "$name");
                    my $ct = 0;
                    while(my $line = <FILE>) {
                        $ct++;
                        print "$name $ct: $line" if $line =~ m/$grep_search_string/;
                    }
                close(FILE);
            }

            if ($arg_grep == 1 && $wildcard && $arg_pwd == 0) {

                if($name =~ &glob2pat($wildcard)) {
                    open (FILE, "$name");
                    my $ct = 0;
                    while(my $line = <FILE>) {
                        $ct++;
                        print "$name: $ct: $line" if $line =~ m/$grep_search_string/;
                    }
                    close(FILE);
                }
            }
            

            if($arg_grep == 1 && !$wildcard && $arg_pwd == 1 ) {

                my $filepath = cwd . "/" . $name;
                open (FILE, "$name");
                my $ct = 0;
                while(my $line = <FILE>) {
                    $ct++;
                    print "$filepath: $ct: $line" if $line =~ m/$grep_search_string/;
                }
                close(FILE);
            }

            if ($arg_grep == 1 && $wildcard && $arg_pwd == 1) {

                if($name =~ &glob2pat($wildcard)) {
                    my $filepath = cwd . "/" . $name;
                    open (FILE, "$name");
                    my $ct = 0;
                    while(my $line = <FILE>) {
                        $ct++;
                        print "$filepath: $ct: $line" if $line =~ m/$grep_search_string/;
                    }
                    close(FILE);
                }
            }
        } 
    }
    chdir($startdir) or die "Unable to change to dir $startdir:$!\n";
}

#Directory is set to current working directory if no arguments given.
if(@ARGV == 0) {
    $ARGV[0] = ".";
}

&scan_directory($ARGV[0]);
exit;


#Fan: not able to pass the test cases: 
#   perl crouse.pl grade/code/python -name "*.py"
#   perl crouse.pl grade -name "*.c" -pwd
#   perl crouse.pl grade -name "??.?" -ls
#   perl crouse.pl grade/code/python -name "*.py" -pwd
#   perl crouse.pl grade -name "s*.c" -grep char -pwd
#   perl crouse.pl grade/code/python -name "*.py" -grep print
#   perl crouse.pl grade -name "*.txt" -grep "^$" -pwd
#   perl crouse.pl grade/code/python -name "*.py" -grep print -pwd