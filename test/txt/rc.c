// Charles Rouse
// CS 312
//  rc.c

#include "rc.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

void read_objs (OBJ_T **list){
  double x, y, z, rad, red, green, blue;
  SPHERE_T sphere;
  OBJ_T *new;
  *list = NULL;
  while (scanf("%lf %lf %lf %lf %lf %lf %lf", &x, &y, &z, &rad, &red, &green, &blue) == 7) { 
    new = (OBJ_T *) malloc(sizeof(OBJ_T));
    new -> sphere.center.x = x;
    new -> sphere.center.y = y;
    new -> sphere.center.z = z;
    new -> sphere.radius = rad;
    new -> color.r = red;
    new -> color.g = green;
    new -> color.b = blue;

	// Pointers to head of linked list
    new -> next = *list;
    *list = new;
   } 
}

int intersect_sphere(RAY_T ray, SPHERE_T sphere, double *t){
    double A = 1;
    double B = 2 * (ray.direction.x * (ray.origin.x - sphere.center.x)
                  + ray.direction.y * (ray.origin.y - sphere.center.y)
                  + ray.direction.z * (ray.origin.z - sphere.center.z));
    double C = ((ray.origin.x - sphere.center.x)*(ray.origin.x - sphere.center.x))
                + ((ray.origin.y - sphere.center.y)*(ray.origin.y - sphere.center.y))
                + ((ray.origin.z - sphere.center.z)*(ray.origin.z - sphere.center.z))
                - (sphere.radius * sphere.radius);
    
    if (((B*B) - 4*A*C) <= 0) {
        return 0;
    }

    double t_0 = (-B + sqrt((B*B) - A*C))/2*A;
    double t_1 = (-B - sqrt((B*B) - A*C))/(2*A);

    if ((t_0 < 0) && (t_1 < 0)) {
        return 0;
    }

    else if (t_0 < t_1) {
        *t = t_0;
    }

    else if(t_1 < t_0){
        *t = t_1;
    }
    
	return 1;
}

COLOR_T cast (RAY_T ray, OBJ_T *list) {
  OBJ_T *curr;                         
  double t;
  double min_t = 1000;
  COLOR_T color;
  color.r = 1;
  color.g = 1;
  color.b = 1;

  for (curr = list; curr != NULL; curr = curr -> next) {
    if (intersect_sphere(ray, curr -> sphere, &t)) {
      if (t < min_t) {
		color = curr -> color;
		min_t = t;
      }
    }
  }
  return color;
}

int main() {
  OBJ_T *list;
  OBJ_T *curr;
  COLOR_T pixel;
  RAY_T ray;
  double t;
  read_objs(&list);
  int x;
  int y;

  printf ("P6\n1000 1000\n255\n");
    
  for (y = 0; y < 1000; y++) {   
    for (x = 0; x < 1000; x++) {
      ray.origin.x = 0;
      ray.origin.y = 0;
      ray.origin.z = 0;
      ray.direction.x = -0.5 + x / 1000.0;
      ray.direction.y = 0.5 - y / 1000.0; 
      ray.direction.z = 1;
      ray.direction = normalize(ray.direction);
      pixel = cast(ray, list);                
      printf ("%c", (unsigned char) (pixel.r * 255)); 
      printf ("%c", (unsigned char) (pixel.g * 255)); 
      printf ("%c", (unsigned char) (pixel.b * 255)); 
    }
  }
  // Free malloc
  for (curr = list; curr != NULL; curr = curr -> next) {
	free(curr);
  }
}
